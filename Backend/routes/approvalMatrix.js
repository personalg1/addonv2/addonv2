const express = require('express');
const router = express.Router();
const approvalMatrixController = require('../controllers/approvalMatrix');

/*Save Purchase Request*/
router.post('/', (req, res) => {
    approvalMatrixController.addDepartment(req.body).then(result => res.send(result))
});

module.exports = router;