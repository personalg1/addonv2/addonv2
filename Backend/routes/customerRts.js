const express = require('express');
const router = express.Router();
const customerController = require('../controllers/customerCtrl');


router.get('/allAcountName', (req, res) => {
	customerController.getAllCustomer(req.body).then( result => res.send(result))
})

/* Add Product to the Database */
router.post('/add', (req, res) => {
	customerController.addCustomer(req.body).then(result => res.send(result));
})

/* View All Materials from the Database */
router.get('/view/all', (req, res) => {
	customerController.viewAllCustomer().then(result => res.send(result));
})

/* View Supplier Materials from the Database */
router.get('/view/:accountId', (req, res) => {
	customerController.viewCustomer(req.params).then(result => res.send(result));
})

router.get('/:accountName', (req, res) => {
	customerController.getSpecificCustomer(req.params).then(result => res.send(result))
})

module.exports = router;