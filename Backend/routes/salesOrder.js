const express = require('express');
const router = express.Router();
const salesOrderController = require('../controllers/salesOrder');

/*Save Sales Order*/
router.post('/', (req, res) => {
    salesOrderController.sendSalesOrder(req.body).then(result => res.send(result))
});

/*list view*/
router.get('/salesOrderMonitoring', (req, res) => {
	salesOrderController.getAllSalesOrder(req.body).then( result => res.send(result))
})

router.get(`/:salesOrderId`, (req, res) => {
    const salesOrderId = req.params.salesOrderId;
    salesOrderController.getbyId({ salesOrderId }).then(result => res.send(result));
})


 /*Fetch Purchase Request for Approval*/

router.get('/salesOrderInPrep', (req, res) => {
    salesOrderController.inPreparation (req.body)  .then(result => res.send(result))
 });

  /*Fetch PSales Ordert for Approval*/
 router.get('/salesOrderForApproval', (req, res) => {
    salesOrderController.forApproval(req.body).then(result => res.send(result))
});


module.exports = router;