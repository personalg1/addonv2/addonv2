const express = require('express');
const router = express.Router();
const productsController = require('../controllers/productsCtrl');


// router.get('/:accountId', (req, res) => {
//     const accountId = req.params.accountId;
//     accountNameController.findById(req.params).then(result => res.send(result));
// })

// /* View Supplier Materials from the Database */
// router.get('/view/:companyId', (req, res) => {
// 	companyController.viewCompany(req.params).then(result => res.send(result));
// })
router.get('/allProducts', (req, res) => {
	productsController.getAllProducts(req.body).then( result => res.send(result))
})

module.exports = router;