const Company = require('../models/company');

// Add a Product to the database
module.exports.addCompany = (reqBody) => {
	let newCompany = new Company ({
		companyId: reqBody.companyId,
		companyName : reqBody.companyName,
		address : reqBody.address
	})


	return newCompany.save().then((user, error) => {
		if(error){
			return `Error: Failed to add the company in the database!`;
		} else {
			return `Successfully added the company in the database!`;
		}
	})

}


// View All Products from the database
module.exports.viewAllCompanies = () => {
	return Company.find().then(result => {
		console.log(result);
	})
}

// View Supplier Products from the database
module.exports.viewCompany = (reqParams) => {
	return Company.find({companyId: reqParams.companyId}).then(result => {
		if(Object.keys(result).length < 1) {
			return `No supplier with the name ${reqParams.companyId}`;
		} else {
			return result
		}
	})
}