const salesOrder = require('../models/SalesOrderDb');

/*Save Sales Order*/
module.exports.sendSalesOrder = (params) => {
	let SalesOrder = new salesOrder({
        // salesOrderId: new Date().getTime(),
		accountName: params.accountName,
        shipTo: params.shipTo,
        paymentTerms: params.paymentTerms,
        requestedDate: params.requestedDate,
        createdOn: params.createdOn,
        // atp: params.atp,
        externalReference: params.externalReference,
        comment: params.comment,
        currency: params.currency,
        postingDate: params.postingDate,
        sapSalesOrderNo: params.sapSalesOrderNo,
        status: params.status,
        Items: params.Items
	})
    console.log(SalesOrder)
	return SalesOrder.save().then((_salesOrder, err) => {
		return (err) ? false : true
	})	
};

/*List View*/

    module.exports.getAllSalesOrder = (params) => {
        return salesOrder.find({}).then( result => {
            return result
        })
    }

    module.exports.getbyId = (params) => {

        return salesOrder.findById(params.salesOrderId).then(salesOrder => {
             return salesOrder }
    
    )} 
        
    

    /*Fetch Purchase Request in Preparation*/
    // module.exports.get = (params) => {
    //     return SalesOrder.find({ Status:"For Approval" })
    //     .then(SalesOrder => {
    //         return result }
    // )}
    
    module.exports.inPreparation = (params) => {

        return salesOrder.find({ Status:"in Preparation" })
    
        .then(salesOrder => {
    
            return salesOrder }
    
    )}
    module.exports.forApproval = (params) => {

        return salesOrder.find({ Status:"for Approval" })
    
        .then(salesOrder => {
    
            return salesOrder }
    
    )}

    

    
  

    
