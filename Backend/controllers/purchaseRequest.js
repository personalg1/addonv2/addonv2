const purchaseRequest = require('../models/purchaseRequest');

/*Save Purchase Request*/
module.exports.sendPurchaseRequest = (params) => {
    let purchaseRequest = new PurchaseRequest({
        Status: params.Status,
        TotalNetValue: params.TotalNetValue,
        CreatedOn: params.CreatedOn,
        CreatedBy: params.CreatedBy,
        ChangedOn: params.ChangedOn,
        SupplierId: params.SupplierId,
        CompanyCode: params.CompanyCode,
        Department: params.Department,
        Items: params.Items,
        Notes: params.Notes,
    })
    console.log(purchaseRequest)
    return purchaseRequest.save().then((_purchaseRequest, err) => {
        return (err) ? false : true
    })  
};
/*Fetch Purchase Request for Approval*/
module.exports.get = (params) => {
    return PurchaseRequest.find({ Status:"In Preparation" })
    .then(purchaseRequest => {
        return purchaseRequest }
)}
/*Fetch Purchase Request in Preparation*/
module.exports.get = (params) => {
    return PurchaseRequest.find({ Status:"for Approval" })
    .then(purchaseRequest => {
        return purchaseRequest }
)}

/*update PurchaseRequest*/
module.exports.editPr = (params) => {
    const update = {
        Status: params.Status,
        TotalNetValue: params.TotalNetValue,
        CreatedOn: params.CreatedOn,
        CreatedBy: params.CreatedBy,
        ChangedOn: params.ChangedOn,
        SupplierId: params.SupplierId,
        CompanyCode: params.CompanyCode,
        Department: params.Department,
        Items: params.Items,
        Notes: params.Notes,
    }
    console.log(update)
    return purchaseRequest.findByIdAndUpdate(params._id, update).then((doc, err) => {
        return (err) ? false : true
    })
}

/*Update Purchase Request*/
/*Delete Purchase Request*/



