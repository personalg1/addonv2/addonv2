const User = require('../models/user')
const auth = require('../auth')
const bcrypt = require("bcrypt"); 

/* Register User */
module.exports.register = (params) => {
	let user = new User({
		username: params.username,
		password: bcrypt.hashSync(params.password,10),
		fullname:params.fullname,
		initialLogin: params.initialLogin,
		employeeId: params.employeeId,
		isAdmin: params.isAdmin,
		isApprover: params.isApprover,
		isRequestor: params.isRequestor,
		department: params.department
	})
	console.log(user)
	return user.save().then((_user, err) => {
		return (err) ? false : true
	})
};
/* Log-in */
module.exports.login = (params) => {

	return User.findOne({username : params.username }).then(user => {
		if (user === null) { return ( "User not Found" ) }

		const isPasswordMatched = bcrypt.compareSync(params.password, user.password)
		
		if (isPasswordMatched) {
			return { accessToken: auth.createAccessToken(user.toObject()) }
		} else {
			return false
		}	
	})
};
/*Authtetication and changing Password*/
module.exports.get = (params) => {
	return User.findById(params.userId).then(user => {

		return user
	})
}
/*Update Password*/
module.exports.passwordchange = (params) => {
	const update = {
	password: bcrypt.hashSync(params.password,10),
	initialLogin: false
	}

	console.log(update)
	return User.findByIdAndUpdate(params.userId, update).then((doc, err) => {
		return (err) ? false : true
	})
}

	


