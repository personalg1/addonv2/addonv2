const express = require('express')
const mongoose = require('mongoose')
const app = express()
const cors = require('cors')

app.use(cors())

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'))
mongoose.connect('mongodb+srv://ortegsssss:spci@cluster0.uswqp.mongodb.net/Dev_Database?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false
})
// mongodb+srv://JoannaMC:Admin@cluster0.wx5pn.mongodb.net/SalesOrderAddOn_DEV?retryWrites=true&w=majority

//Development: mongodb+srv://ortegsssss:spci@cluster0.uswqp.mongodb.net/Dev_Database?retryWrites=true&w=majority
//Testing: mongodb+srv://ortegsssss:spci@cluster0.uswqp.mongodb.net/Test_Database?retryWrites=true&w=majority



app.use(express.json())
app.use(express.urlencoded({ extended: true }))

const userRoutes = require('./routes/user')
const salesOrderRoutes = require('./routes/salesOrder')
const customerRoutes = require('./routes/customerRts')
const productsRoutes = require('./routes/productRts')
const companyRoutes = require('./routes/customerRts')
const currencyRoutes = require('./routes/currencyRts')
const uomRoutes = require('./routes/uomRts')
// const approvalMatrixRoutes = require('.routes/approvalMatrix')

app.use('/api/users', userRoutes)
app.use('/api/salesOrder', salesOrderRoutes)
app.use('/api/customer', customerRoutes)
app.use('/api/products', productsRoutes)
app.use('/company', companyRoutes)
app.use('/api/currency', currencyRoutes)
app.use('/api/uom', uomRoutes)
// app.use('/api/approvalMatrix', approvalMatrixRoutes)

app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
})
