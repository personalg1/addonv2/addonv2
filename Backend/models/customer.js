const mongoose = require('mongoose')

const CustomerSchema = new mongoose.Schema({
        accountId: {
            type: String,
            required: true
        },
        accountName:{
            type: String,
            required: true
        },
        address: {
            type: String,
            required: true
        },
        paymentTerms:{
            type: String,
            required: true
        }

    })

    module.exports = mongoose.model('Customer', CustomerSchema);
