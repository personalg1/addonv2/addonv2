const mongoose = require('mongoose')

const SalesOrderSchema = new mongoose.Schema({
        accountName: {
            type: String,
            required: true
        },
        shipTo: {
            type: String,
            required: true
        },
        paymentTerms: {
            type: String,
            required: true
        },
        requestedDate: {
            type: Date, // date format 2021-10-27 23:30
            required: true
        },
        createdOn: {
            type: Date, // date format 2021-10-27 23:30
            default: Date.now
        },
        externalReference:{
            type: String,
        comments: String
        },
        currency: {
            type: String,
            required: true
        },
        postingDate: {
            type: String, // date format 2021-10-27 23:30
        },
        salesOrderId: {
            type: String,
        },
        sapSalesOrderNo: {
            type: String,
        },
        status: {
            type: String,
        },
        Items: []
    })

module.exports = mongoose.model('salesOrder', SalesOrderSchema);