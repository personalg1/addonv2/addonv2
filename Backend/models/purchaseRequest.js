const mongoose = require('mongoose')

const purchaseRequestSchema = new mongoose.Schema({
    Status: {
        type: String
    },
    TotalNetValue: {
        type: Number
    },
    CreatedOn: {
        type: Date,
        default: new Date("<YYYY-mm-dd>")
    },
    CreatedBy: {
        type: String
    },
    ChangedOn: {
        type: Date,
        default: new Date("<YYYY-mm-dd>")
    },
    SupplierId:{
        type: String
    },
    CompanyCode: {
        type: String
    },
    Department: {
        type: String
    },
    Items: [],
    Notes: {
        type: String
    }
    })

module.exports = mongoose.model('purchaseRequest', purchaseRequestSchema);