const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: [true, 'Username is required.']
    },
    fullname:{
        type: String,
        required: [true, 'Fullname is required. ']
    },
    employeeId:{
        type: String,
        required: [true, 'Fullname is required. ']
    },
    password: {
        type: String,
        required: [true, 'Password is required.']
    },
    initialLogin: {
        type: Boolean,
        default: true
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    isApprover: {
        type: Boolean,
        default: false
    },
    isRequestor: {
        type: Boolean,
        default: false
    },
    department: {
        type: String
    }
})
module.exports = mongoose.model('user', userSchema);