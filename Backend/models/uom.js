const mongoose = require('mongoose')

const uomSchema = new mongoose.Schema({
    code: {
        type: String
    },
    description: {
        type: String
    }
})

module.exports = mongoose.model('Uom', uomSchema);