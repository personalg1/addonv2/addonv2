let SalesOrderMenu = document.querySelector("#SalesOrderMenu")

if((localStorage.getItem("isApprover") == "true")) {
  
  let pRmenuContainer = document.querySelector(".pRmenuContainer")

  pRmenuContainer.innerHTML =  `<a href="#">Sales Order Menu</a>`

	SalesOrderMenu.innerHTML = 
	`
  <div class="btnContainer">
      <Button class="iconBtn" onclick="salesOrderMonitoring()">
        <div class="btnHeader">Sales Order Monitoring</div>

        <img src="./assets/image/icons8-purchase-order-60.png"></div>
        <div class=count>0</div>
      </Button>

      <Button class="iconBtn">
        <div class="btnHeader" onclick="salesOrderMonitoring">Sales Order Approval</div>
    
        <img src="./assets/image/icons8-purchase-order-60.png"></div>
        <div class=count>0</div>
      </Button>
  </div>
	`
  let count = document.querySelector(".count");
  let token = localStorage.getItem("token");

  fetch(`https://fierce-plains-63425.herokuapp.com/api/salesOrder/salesOrderMonitoring`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
		})
		.then(res => res.json())
		.then(data => {
      console.log(data.length)
      count.innerHTML = data.length
    }
    )
} else {

  let pRmenuContainer = document.querySelector(".pRmenuContainer")

  pRmenuContainer.innerHTML =  `<a href="#">Sales Order Menu</a>`

	SalesOrderMenu.innerHTML =
	`
  <div class="btnContainer">
      <Button class="iconBtn" onclick="salesOrderMonitoring()">
        <div class="btnHeader">Sales Order Monitoring</div>
          <div>  
            <img src="./assets/image/icons8-purchase-order-60.png">
          </div>
          <div class=count>0</div>
      </Button>
        </div>
  </div>
	
  <div class="btnContainer">
      <Button class="iconBtn" onclick="salesOrderMonitoring()">
      <div class="btnHeader">Sales Order Posted</div>
    <div>  
         <img src="./assets/image/icons8-purchase-order-60.png"></div>
    <div class=count>0</div>
    </Button>
    </div>
  </div>
	`
  let count = document.querySelector(".count");
  let token = localStorage.getItem("token");

  fetch(`https://fierce-plains-63425.herokuapp.com/api/salesOrder/salesOrderMonitoring`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
		})
		.then(res => res.json())
		.then(data => {
      console.log(data.length)
      count.innerHTML = data.length
    }
    )


}

let UserMaintenance = document.querySelector("#UserMaintenance")

if ((localStorage.getItem("isAdmin") == "true")) {

 let uMmenuContainer = document.querySelector(".uMmenuContainer")

  uMmenuContainer.innerHTML =  `<a href="#">User Maintenance</a>`

  UserMaintenance.innerHTML = 
    `
    <div class="btnContainer">
        <Button class="iconBtn" onclick="">
          <div class="btnHeader">User Maintenance</div>
      <div>
          <img src="./assets/image/icons8-purchase-order-60.png"></div>
      <div class=count>0</div></Button>
    </div>
  
    `
  } else {
  UserMaintenance.innerHTML = 
  `
  
  `
}

function salesOrderCreation(){
location.replace("./salesOrderCreation.html")
}

function salesOrderMonitoring(){
  location.replace("./salesOrderMonitoring.html")
}

