let changePasswordForm = document.querySelector("#changePasswordForm")
;
console.log(localStorage.getItem("id"));
console.log(localStorage.getItem("username"));
console.log(localStorage.getItem("password"));
console.log(localStorage.getItem("fullname"));
console.log(localStorage.getItem("initialLogin"));
console.log(localStorage.getItem("isAdmin"));
console.log(localStorage.getItem("token"));
console.log(localStorage.getItem("department"));

document.getElementById("username").innerHTML = localStorage.getItem("username");

changePasswordForm.addEventListener("submit", (e) => {
	e.preventDefault();

	let userId = localStorage.getItem("id");
    let passwordCurrent = document.querySelector("#passwordCurrent").value; 
    let passwordNew = document.querySelector("#passwordNew").value;
    let passwordConfirm = document.querySelector("#passwordConfirm").value;
    let token = localStorage.getItem('token')

    if (passwordConfirm == "" || passwordNew == "" || passwordCurrent == ""){
		alert("Enter All Requirements")
	} else { 
       if ( passwordConfirm !== passwordNew  && passwordCurrent !== passwordCurrent)
        alert ("Current or Confirm Password does not Match")

        fetch(`https://fierce-plains-63425.herokuapp.com/api/users/changePassword`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				userId,
				password: passwordConfirm
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true) {

				alert('Password Changed!')
				window.location.replace("./launchpad.html")
			} else {
				
				alert("Something went wrong.")

			}
		})
    } 


})


