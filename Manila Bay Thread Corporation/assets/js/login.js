let loginForm = document.querySelector("#loginForm");

loginForm.addEventListener("submit", (e) => {
	e.preventDefault();

	let username = document.querySelector("#username").value;
	let password = document.querySelector("#password").value;

	if (username == "" || password == ""){
		alert("Please input your username and/or password.")
	} else { 
		fetch('https://fierce-plains-63425.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				username: username,
				password: password
			})
		})
		.then(res => {
			return res.json()
		})
// 	
.then(data => {
	console.log(data)
	if(data.accessToken) {
		//store JWT in local storage
		localStorage.setItem('token', data.accessToken);
		//send fetch request to decode JWT and obtain user ID and role for storing in context
		fetch(`https://fierce-plains-63425.herokuapp.com/api/users/details`, {
			headers: {
				Authorization: `Bearer ${data.accessToken}`
			}
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			//set the global user to have properties containing authenticated user's id and role
			localStorage.setItem("id", data._id);
			localStorage.setItem("username", data.username);
			localStorage.setItem("fullname", data.fullname);
			localStorage.setItem("isAdmin", data.isAdmin);
			localStorage.setItem("initialLogin", data.initialLogin);
			localStorage.setItem("department", data.department);
			localStorage.setItem("employeeId", data.employeeId);
			localStorage.setItem("isApprover", data.isApprover);
			localStorage.setItem("isRequestor", data.isRequestor);

			console.log(data)

				if (data.initialLogin == true)
				{
					window.location.replace("./changepassword.html")
					alert ("Initial Login Must Change Update the Password")
				} else {
					window.location.replace("./launchpad.html")
					alert ("Welcome" + " " + `${data.fullname}`)
				}
		})
	} else {
		//authentication failure
		alert("Something went wrong!");
	}
})
}

})
