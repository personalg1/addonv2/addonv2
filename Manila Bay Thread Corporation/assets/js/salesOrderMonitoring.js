console.log(localStorage.getItem("username"));
console.log(localStorage.getItem("password"));
console.log(localStorage.getItem("fullname"));
console.log(localStorage.getItem("initialLogin"));
console.log(localStorage.getItem("isAdmin"));
console.log(localStorage.getItem("token"));

let salesOrderMonitoring = document.querySelector(".buttonRow")

if(localStorage.getItem("isApprover") == true) {
	salesOrderMonitoring.innerHTML = 
	`
	<button>Release</button>
    <button>Approve</button>
	<button type="button" class="closeButton">Close</button>
	<button type="button" class="New">New</button>
    

	`
} else {
	salesOrderMonitoring.innerHTML =
	`
	<button type="button" class="closeButton">Close</button>
	<button type="button" class="New">New</button>
    <button type="button" class="Post">Post</button>
	`
}

function toggle(source) {
    var checkboxes = document.querySelectorAll('input[type="checkbox"]');
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i] != source)
            checkboxes[i].checked = source.checked;
    }
}

function newsalesOrder(){
    location.replace("./salesOrderCreation.html")
}

let newSo = document.querySelector(".New")
newSo.onclick = newsalesOrder;

let salesOrderTable = document.querySelector("#salesOrderTable")
let salesOrder;
let token = localStorage.getItem("token") 
// Close function
function Close(){
	let confirm = window.confirm ("Return to Launchpad? All unsaved data will be deleted");
	if (confirm == true){
		 window.location.replace('./launchpad.html')
	}		
}

let close = document.querySelector(".closeButton")
close.onclick = Close;
//

	
if (localStorage.getItem("isApprover") == "true"){

    fetch(`https://fierce-plains-63425.herokuapp.com/api/salesOrder/salesOrderMonitoring`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
            }
    })
    .then(res => res.json())
    .then(data => {
        if (data.length < 1){
            salesOrder = `<h4 text-align: center >No Sales Order for Approval Available</h4>`
            salesOrderTable.innerHTML = salesOrder;
        } else {

            // console.log('test1');
            salesOrder = data.map(salesOrderData=> {            
            return( 
            

                salesOrder =
                `
                <tr>
                    <td><input type="checkbox" value="selectAll" id="flexCheckDefault"></td>
                    <td><a href='salesOrderView.html?id=${salesOrderData._id}'>${salesOrderData._id} </a></td>
                    <td>${salesOrderData.sapSalesOrderNo}</td>
                    <td>${salesOrderData.createdOn}</td>
                    <td>${salesOrderData.requestedDate}</td>
                    <td>${salesOrderData.externalReference}</td>
                </tr>
                `
                )

            }).join("");
            salesOrderTable.innerHTML = salesOrder;    
    }
    })
} else {

    fetch(`https://fierce-plains-63425.herokuapp.com/api/salesOrder/salesOrderMonitoring`, {
     method: 'GET',
     headers: {
         'Content-Type': 'application/json',
         'Authorization': `Bearer ${token}`
         }
 })
 .then(res => res.json())
 .then(data => {
 	// console.log(data)

     if (data.length < 1){
        salesOrder = `<h1>No Sales Order in Preparation Available</h1>`
         salesOrderTable.innerHTML = salesOrder;
     } else {
        salesOrder = data.map(salesOrder=> {
            console.log('salesOrder', salesOrder)
         return(
            salesOrder =
             `
             <tr>
                <td style="width: 6%">  <input type="checkbox" value="selectAll" id="flexCheckDefault"></td>
                <td>
                <a href='salesOrderCreation.html?id=${salesOrder._id}'>${salesOrder._id} </a></td>
                <td>${salesOrder.sapSalesOrderNo}</td>
                <td>${salesOrder.createdOn}</td>
                <td>${salesOrder.requestedDate}</td>
                <td>${salesOrder.externalReference}</td>
             </tr>
             `
             )
         }).join("");
         salesOrderTable.innerHTML = salesOrder;                       
 }
 })

}