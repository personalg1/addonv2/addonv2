//detailed view


let params = new URLSearchParams(window.location.search)
let salesOrderid = params.get('salesOrderid')
let token = localStorage.getItem('token')


 console.log(salesOrderid)
 
fetch(`https://fierce-plains-63425.herokuapp.com/api/salesOrderid/${sOid}`)
.then(res => res.json())
.then(data => {
	console.log(data)

	let AccountName = document.querySelector(".accountName");
	let Status = document.querySelector(".status");
	let ShipTo = document.querySelector(".shipTo");
    let CreatedOn = document.querySelector(".createdOn");
    let PaymentTerms = document.querySelector(".paymentTerms");
    let Currency = document.querySelector(".currency");
    let RequestedDate = document.querySelector(".requestedDate");
    let PostingDate = document.querySelector(".postingDate");
    let ExternalReference = document.querySelector(".externalReference");
    let SapSalesOrderNo = document.querySelector(".sapSalesOrderNo");
    let Comments = document.querySelector(".comments");
   

    AccountName.value = data.AccountName;
    Status.value = data.Status;
    ShipTo.value = data.ShipTo;
    CreatedOn.value = data.CreatedOn;
    PaymentTerms.value = data.PaymentTerms;
    Currency.value = data.Currency;
    RequestedDate.value = data.RequestedDate;
    PostingDate.value = data.PostingDate;
    ExternalReference.value = data.ExternalReference;
    SapSalesOrderNo.value = data.SapSalesOrderNo;
    Comments.value = data.Comments;

console.log(AccountName)
});

    // Logout function
function logOut(){
	let confirm = window.confirm ("Do you want to Log Out?");
	if (confirm == true){
		 localStorage.clear();
		 window.location.replace('./logout.html')
	}		
}

let purchaseRequestMenu = document.querySelector(".buttonRow")

if(localStorage.getItem("isApprover") == true) {
	purchaseRequestMenu.innerHTML = 
	`
	<button>Release</button>
	<button type="button" class="Save">Save</button>
	<button type="button" class="Approval">Submit for Approval</button>
	<button type="button" class="closeButton">Close</button>
	<button type="button">New</button>

	`
} else {
	purchaseRequestMenu.innerHTML =
	`
	<button type="submit" class="Submit">Save</button>
	<button type="button" class="Approval">Submit for Approval</button>
	<button type="button" class="closeButton">Close</button>
	`
}
// Close function
function Close(){
	let confirm = window.confirm ("Do you want to close? All unsaved data will be deleted");
	if (confirm == true){
		 window.location.replace('./salesOrderMonitoring.html')
	}		
}

let close = document.querySelector(".closeButton")
close.onclick = Close;


function itemsDisplay(){
	let a = 0;
	itemsList = itemsArray.map(itemsList=> {
        return(
			a++,
            itemsList =
            `
            <tr>
				<td><button type="button" class="Edit">Edit</button></td>
				<td>${a}</td>
				<td><input type="text" value="${itemsList.ItemType}" disabled></td>
				<td><input type="text" value="${itemsList.ProductId}" disabled></td>
				<td><input type="text" value="${itemsList.ProductDescription}" disabled></td>
				<td><input type="text" value="${itemsList.ShiptoLocationId}" disabled></td>
				<td><input type="text" value="${itemsList.ShipToServiceLocationDescription}" disabled></td>
				<td><input type="text" value="${itemsList.DeliveryDate}" disabled></td>
				<td><input type="text" value="${itemsList.Quantity}" disabled></td>
				<td><input type="text" value="${itemsList.Unit}" disabled></td>
            </tr>
            `
            )
        }).join("");
        productsContainer.innerHTML = itemsList; 
}