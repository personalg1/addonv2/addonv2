

// Logout function
function logOut(){
	let confirm = window.confirm ("Do you want to Log Out?");
	if (confirm == true){
		 localStorage.clear();
		 window.location.replace('./logout.html')
	}		
}

let purchaseRequestMenu = document.querySelector(".buttonRow")

if(localStorage.getItem("isApprover") == true) {
	purchaseRequestMenu.innerHTML = 
	`
	<button>Release</button>
	<button type="button" class="Save">Save</button>
	<button type="button" class="Approval">Submit for Approval</button>
	<button type="button" class="closeButton">Close</button>
	<button type="button">New</button>

	`
} else {
	purchaseRequestMenu.innerHTML =
	`
	<button type="submit" class="Submit">Save</button>
	<button type="button" class="Approval">Submit for Approval</button>
	<button type="button" class="closeButton">Close</button>
	`
}

{/* <button type="button" class="Approval">Submit for Approval</button> */}

let userIcon = document.querySelector(".userIcon")
userIcon.onclick = logOut;

// Close function
function Close(){
	let confirm = window.confirm ("Do you want to close? All unsaved data will be deleted");
	if (confirm == true){
		 window.location.replace('./salesOrderMonitoring.html')
	}		
}

let close = document.querySelector(".closeButton")
close.onclick = Close;


//Array Push

let x = 0;
const itemsArray = [];
const itemsArraySO = [];


function itemsPush(){	
itemsArray.push({
	ProductId: document.querySelector(".ProductId").value,
	ProductDescription: document.querySelector(".ProductDescription").innerHTML,
	Quantity: document.querySelector(".Quantity").innerHTML,
	Unit: document.querySelector(".Unit").value,
});
x++;
console.log(itemsArray)
}

function itemsPushSO(){	
	itemsArraySO.push({
		lineNo: document.querySelector(".lineNo").value,
		productId: document.querySelector(".productId").value,
		productDescription: document.querySelector(".productDescription").value,
		quantity: document.querySelector(".quantity").value,
		unitMeasure: document.querySelector(".unitMeasure").value,
		unitPrice: document.querySelector(".unitPrice").value,
		totalAmount: document.querySelector(".totalAmount").value,
		
	});
	x++;
	console.log(itemsArraySO)
	}

// display items
let productsContainer = document.getElementById("ItemsArray");
let itemsList;


function itemsDisplay(){
	let a = 0;
	itemsList = itemsArray.map(itemsList=> {
        return(
			a++,
            itemsList =
            `
            <tr>
            <td><button type="button" class="Edit">Edit</button></td>
			<td>${a}</td>
            <td><input type="text" value="${itemsList.ItemType}" disabled></td>
            <td><input type="text" value="${itemsList.ProductId}" disabled></td>
            <td><input type="text" value="${itemsList.ProductDescription}" disabled></td>
            <td><input type="text" value="${itemsList.ShiptoLocationId}" disabled></td>
			<td><input type="text" value="${itemsList.ShipToServiceLocationDescription}" disabled></td>
			<td><input type="text" value="${itemsList.DeliveryDate}" disabled></td>
			<td><input type="text" value="${itemsList.Quantity}" disabled></td>
			<td><input type="text" value="${itemsList.Unit}" disabled></td>

			<td>
			<div class="d-flex justify-content-center">
				<!-- <button class="crudButton button btn me-1" type="button" onclick="create_tr('table_body')">
					<i class="fas fa-plus">+</i>
				</button> -->
				<button class="crudButton button btn ms-1" type="button" onclick="remove_tr(this)">
					<i class="far fa-trash-alt">-</i>
				</button>
			</div>
		</td>
            </tr>
            `
            )
        }).join("");
        productsContainer.innerHTML = itemsList; 
}

// console.log(today)


let salesOrderForm = document.querySelector("#salesOrderForm");
salesOrderForm.addEventListener("submit", (e) => {
	e.preventDefault();

	let accountName = document.querySelector("#accountName").value;
	let shipTo = document.querySelector("#shipTo").value;
	let paymentTerms = document.querySelector("#paymentTerms").value;
	let currency = document.querySelector("#currency").value;
	let requestedDate = document.querySelector("#requestedDate").value;
	let postingDate = document.querySelector("#postingDate").value;
	// let atp = document.querySelector("#atp").value;
	let sapSalesOrderNo = document.querySelector("#sapSalesOrderNo").value;
	let externalReference = document.querySelector("#externalReference").value;
	// let status = document.querySelector("#status").value;
	let status = "For Approval";
	let comment = document.querySelector("#comment").value;
	
	if (localStorage.getItem("isAdmin") == ""){
		alert("Please input data to all fields")
	} else { 
		fetch('https://fierce-plains-63425.herokuapp.com/api/salesOrder/', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				accountName: accountName,
				shipTo: shipTo,
				paymentTerms: paymentTerms,
				currency: currency,
				requestedDate: requestedDate,
				postingDate: postingDate,
				sapSalesOrderNo: sapSalesOrderNo,
				externalReference: externalReference,
				status: status,
				comment: comment,
				Items: itemsArray,
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			if(data = true) {
				alert("Sales Order Submitted for Aprroval")			
			} else {
				alert("Something went wrong!");
			}
		})
	}

})

//Adds a row to the new sales order table for orders consisting of multiple products
function create_tr(table_id) {
    let table_body = document.getElementById(table_id);
        first_tr   = table_body.firstElementChild;
	
	var table = $('#table_body');
	// console.log($('table tbody tr:first').clone());
	var appendedTable = $('table tbody tr:first').clone().appendTo(table);
	appendedTable.find("input[type='text']").val("");
	appendedTable.find('.number').html(table_body.rows.length);
    //     tr_clone   = first_tr.clone().find("input[type='text']").val("");
	// tr_clone.querySelector('.number').innerHTML = table_body.rows.length+1;
    // table.append('<tr><td>1</td></tr>');

    clean_first_tr(table_body.lastElementChild);
}

// Deletes the corresponding row in the new sales order table except if there is only 1 left
function remove_tr(This) {
    if(This.closest('tbody').childElementCount > 1)
    {
        This.closest('tr').remove();
    }
}



//SAVE DATA
// let Save = document.querySelector(".Save");
// Save.addEventListener("click",  () => {
// 	let accountName = document.querySelector("#accountName").value;
// 	let shipTo = document.querySelector("#shipTo").value;
// 	let paymentTerms = document.querySelector("#paymentTerms").value;
// 	let currency = document.querySelector("#currency").value;
// 	let requestedDate = document.querySelector("#requestedDate").value;
// 	let postingDate = document.querySelector("#postingDate").value;
// 	let sapSalesOrderNo = document.querySelector("#sapSalesOrderNo").value;
// 	let externalReference = document.querySelector("#externalReference").value;
// 	let status = "For Approval";
// 	let comment = document.querySelector("#comment").value;
// 	conso
// 	if (localStorage.getItem("isAdmin") == ""){
// 		alert("Please input data to all fields")
// 	} else { 
// 		fetch('https://fierce-plains-63425.herokuapp.com/api/salesOrder/', {
// 			method: 'POST',
// 			headers: {
// 				'Content-Type': 'application/json'
// 			},
// 			body: JSON.stringify({
// 				accountName: accountName,
// 				shipTo: shipTo,
// 				paymentTerms: paymentTerms,
// 				currency: currency,
// 				requestedDate: requestedDate,
// 				postingDate: postingDate,
// 				sapSalesOrderNo: sapSalesOrderNo,
// 				externalReference: externalReference,
// 				status: status,
// 				comment: comment,
// 				Items: itemsArray,
// 			})
// 		})
// 		.then(res => {
// 			return res.json()
// 		})
// 		.then(data => {
// 			if(data = true) {
// 				alert("Sales Order Saved")	
// 				window.location.replace('./salesOrderMonitoring.html')		
// 			} else {
// 				alert("Something went wrong!");
// 			}
// 		})
// 	}

//LOOK UP ACCOUNT NAME
let selectedAccount = document.getElementById('AccountName')
console.log(selectedAccount);
let accountContainer = document.getElementById("accounts")
let account;

function fetchCompanies() {
	if(selectedAccount.value === ""){
		fetch('https://fierce-plains-63425.herokuapp.com/api/customer/allAccountName', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(res => res.json()).then(data => {
			console.log(data);
			account = data.map(result => {
				return  (
					account =
					`
					<option value="${result.accountName}">
					${result.accountId}
					</option>
					`
				)
			})
			accountContainer.innerHTML = account;
		}) 
	}
}

fetchCompanies();

let shippingLocation = document.getElementById("shipTo")
let terms = document.getElementById("paymentTerms");

function callbackOne() {
	if(selectedAccount.value.length > 0) {
		fetch(`https://fierce-plains-63425.herokuapp.com/api/customer/${selectedAccount.value}`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(res => res.json()).then(data => {
			shippingLocation.value = data.address
			terms.value = data.paymentTerms
		})
	}
}

document.getElementById('AccountName').oninput = function() {
	callbackOne()
};

let date = document.getElementById('createdOn');
window.onload = (e) => {
	const month = new Date().toLocaleString('default', { month: 'long' })
	const day = new Date().getDate();
	const year = new Date().getFullYear();

	date.value = `${day} ${month} ${year}`
}

let currencyContainer = document.getElementById('currencies')
// let currency;
fetch('https://fierce-plains-63425.herokuapp.com/api/currency/view/all', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json'
	}
}).then(res => res.json()).then(data => {
	// console.log(data);
	currency = data.map(result => {
		return  (
			currency =
			`
			<option value="${result.currencyTicker.toUpperCase()}">
			${result.currencyName}
			</option>
			`
		)
	})
	currencyContainer.innerHTML = currency;
}) 

let productIdContainer = document.getElementById('products')
let idProduct;
fetch('https://fierce-plains-63425.herokuapp.com/api/products/view/allProducts', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json'
	}
}).then(res => res.json()).then(data => {
	// console.log(data);
	idProduct = data.map(result => {
		return  (
			idProduct =
			`
			<option value="${result.materialId}">
			${result.materialDescription}
			</option>
			`
		)
	})
	productIdContainer.innerHTML = idProduct;
})

let selectedProduct = document.getElementById('product');
let prodDesc = document.getElementById("productDescription");
let unitPrice = document.getElementById("unitPrice");

function callbackTwo() {
	if(selectedProduct.value.length > 0) {
		fetch(`https://fierce-plains-63425.herokuapp.com/api/products/${selectedProduct.value}`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(res => res.json()).then(data => {
			prodDesc.value = data.materialDescription
			unitPrice.value = data.price
		})
	}
}

document.getElementById('product').oninput = function() {
	callbackTwo()
};


let uomContainer = document.getElementById('uoms')
let uom;
fetch('https://fierce-plains-63425.herokuapp.com/api/uom/view/all', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json'
	}
}).then(res => res.json()).then(data => {
	// console.log(data);
	uom = data.map(result => {
		return  (
			uom =
			`
			<option value="${result.code}">
			${result.description}
			</option>
			`
		)
	})
	uomContainer.innerHTML = uom;
})

document.getElementById('quantity').oninput = function() {
	callbackThree()
};

function callbackThree() {
	let quantityValue = document.getElementById('quantity').value
	console.log(quantityValue);
	let priceValue = document.getElementById('unitPrice').value
	console.log(priceValue);
	let totalAmountContainer = document.getElementById('totalAmount')

	totalAmountContainer.value = quantityValue * priceValue
}

;